# NW.js with Vue and JavaScript

> A NW.js & Vue.js quick start boilerplate

### Getting Started

#### Installation

1. Install vue-cli if you not have

``` bash
npm install -g vue-cli
```
2. Scaffold boilerplate:

``` bash
vue init gitlab:patrickalima98/nwjs-with-vue-and-javascript <project name>
```
#### Development

Specify target NW.js version in `package.json`:

You can find available options [here](https://github.com/evshiron/nwjs-builder-phoenix).

``` json
{
  [...]
  "build": {
    [...]
    "nwVersion": "0.32.1",
    [...]
  },
  [...]
}
```

Run NW.js application for development:

``` bash
npm start
```